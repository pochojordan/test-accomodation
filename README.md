# test-accomodation

Este proyecto está realizado con Vite + React + Typescript + Tailwind, para optimizar la configuración global y el compilado.

Los comandos para ejecutar el proyecto son:

    - npm install
    - npm run dev

**_Autor: Jose Jordán_**
