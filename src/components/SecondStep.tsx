import React from "react";
import { useForm } from "react-hook-form";

interface SSProps {
    nextStep: () => void;
    updateForm: (e) => void;
    form: object;
}

export default function SecondStep({ nextStep, form, updateForm }: SSProps) {
    const {
        register,
        handleSubmit,
        formState: { errors, isDirty, isValid },
    } = useForm({ mode: "onChange" });

    // solo se hace cuando todos los campos son correctos
    const onSubmit = (e) => {
        updateForm(e);
        nextStep();
    };

    return (
        <div className="flex flex-col flex-1 overflow-hidden gap-y-2 step">
            <form className="flex flex-1" onSubmit={handleSubmit(onSubmit)}>
                <div className="flex flex-col flex-1 gap-y-6 module">
                    <div className="flex flex-col flex-1 gap-y-2 container-form">
                        <h2 className="title text-2xl">Owner</h2>
                        <label htmlFor="owner_name">Name*</label>
                        <input
                            className="input-class"
                            type="text"
                            name="owner_name"
                            id=""
                            placeholder="Jose"
                            defaultValue={form.owner_name}
                            {...register("owner_name", {
                                required: true,
                                minLength: 4,
                                maxLength: 64,
                            })}
                        />
                        {errors.owner_name?.type === "minLength" && (
                            <span className="text-sm ml-2">
                                *This name is too short
                            </span>
                        )}
                        {errors.owner_name?.type === "maxLength" && (
                            <span className="text-sm ml-2">
                                *This name is too long
                            </span>
                        )}
                        <label htmlFor="owner_email">Email*</label>
                        <input
                            className="input-class"
                            type="text"
                            name="owner_email"
                            id=""
                            placeholder="example@domain.es"
                            defaultValue={form.owner_email}
                            {...register("owner_email", {
                                required: true,
                                pattern:
                                    /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})$/,
                            })}
                        />
                        {errors.owner_email?.type === "required" && (
                            <span className="text-sm ml-2">
                                *This field is required
                            </span>
                        )}
                        {errors.owner_email?.type === "pattern" && (
                            <span className="text-sm ml-2">
                                *The format is not correct
                            </span>
                        )}
                        <label htmlFor="owner_phone">Phone</label>
                        <input
                            className="input-class"
                            type="tel"
                            name="owner_phone"
                            id=""
                            placeholder="666555222"
                            defaultValue={form.owner_phone}
                            {...register("owner_phone", {
                                minLength: 9,
                                pattern: /^[0-9]+$/i,
                            })}
                            onKeyPress={(event) => {
                                if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                }
                            }}
                        />
                        {errors.owner_phone?.type === "minLength" && (
                            <span className="text-sm ml-2">
                                *Phone could have 9 digits at least
                            </span>
                        )}
                    </div>
                    <button
                        className="btn-class mt-auto w-1/2 self-center"
                        type="submit"
                        disabled={!isDirty || !isValid}
                    >
                        Next step
                    </button>
                </div>
            </form>
        </div>
    );
}
