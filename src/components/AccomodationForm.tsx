import React from "react";

import { useState } from "react";

import FirstStep from "/src/components/FirstStep";
import SecondStep from "/src/components/SecondStep";
import ThirdStep from "/src/components/ThirdStep";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default function AccomodationForm() {
    // pasos
    const [step, setStep] = useState(0);

    // funcion para pasar entre pasos
    const nextStep = () => {
        setStep((step) => {
            return step == 2 ? 0 : step + 1;
        });
    };

    // form
    const [formData, setForm] = useState({});

    // funcion para actualizar el form
    const updateForm = (form) => {
        setForm({ ...formData, ...form });
    };

    return (
        <div className="flex flex-col h-full w-full justify-center overflow-hidden">
            {/* log */}
            {/* <pre>{JSON.stringify(formData)}</pre> */}

            <div className="flex flex-col self-center lg:w-min lg:h-min w-full h-full min-h-10/12 min-w-8/12 overflow-hidden">
                {step == 0 ? (
                    <FirstStep
                        form={formData}
                        nextStep={nextStep}
                        updateForm={updateForm}
                    ></FirstStep>
                ) : null}
                {step == 1 ? (
                    <SecondStep
                        form={formData}
                        nextStep={nextStep}
                        updateForm={updateForm}
                    ></SecondStep>
                ) : null}
                {step == 2 ? (
                    <ThirdStep form={formData} nextStep={nextStep}></ThirdStep>
                ) : null}
            </div>

            {/* notificacion toast */}
            <ToastContainer />
        </div>
    );
}
