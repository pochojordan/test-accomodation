import React from "react";
import { useForm } from "react-hook-form";
import { useRef, useState } from "react";

import { showNotification } from "/src/utils/global.js";

interface SSProps {
    nextStep: () => void;
    updateForm: (e) => void;
    form: object;
}

export default function FirstStep({ nextStep, form, updateForm }: SSProps) {
    const {
        register,
        handleSubmit,
        formState: { errors, isDirty, isValid },
    } = useForm({ mode: "onChange" });

    // array de tipos
    const types = ["apartment", "villa", "house"];

    // solo se hace cuando todos los campos son correctos
    const onSubmit = (e) => {
        updateForm({ ...e, images: [...images] });
        nextStep();
    };

    // trabajar con imagen

    // array de tipos
    const [images, setImages] = useState([]);

    const hiddenInput = useRef(null);

    const handleClick = () => {
        hiddenInput.current.click();
    };

    const loadImage = (files) => {
        const filesArr: Array<string> = [];

        const max = images.length + Object.keys(files).length;
        if (max > 2) {
            showNotification(false, "Max 2 photos");
            return;
        }

        Object.keys(files).map((file) => {
            const src = URL.createObjectURL(files[file]);
            filesArr.push(src);

            setImages([...images, ...filesArr]);
        });
    };

    return (
        <div className="flex flex-col flex-1 overflow-hidden gap-y-2 step">
            <form
                className="flex flex-1 overflow-hidden"
                onSubmit={handleSubmit(onSubmit)}
            >
                <div className="flex flex-col flex-1 gap-y-6 module overflow-hidden">
                    <div className="flex flex-col flex-1 gap-y-2 container-form overflow-hidden">
                        <h2 className="title text-2xl">Accomodation</h2>
                        <label htmlFor="name">Name*</label>
                        <input
                            className="input-class"
                            type="text"
                            name="name"
                            id=""
                            placeholder="Jose's House"
                            defaultValue={form.name}
                            {...register("name", {
                                required: true,
                                minLength: 4,
                                maxLength: 128,
                                pattern: /^[A-Za-z\s]+$/i,
                            })}
                        />
                        {errors.name?.type === "required" && (
                            <span className="text-sm ml-2">
                                *This field is required
                            </span>
                        )}
                        {errors.name?.type === "pattern" && (
                            <span className="text-sm ml-2">
                                *Numbers are not allowed
                            </span>
                        )}
                        {errors.name?.type === "minLength" && (
                            <span className="text-sm ml-2">
                                *This name is too short
                            </span>
                        )}
                        {errors.name?.type === "maxLength" && (
                            <span className="text-sm ml-2">
                                *This name is too long
                            </span>
                        )}
                        <label htmlFor="address">Address*</label>
                        <input
                            className="input-class"
                            type="text"
                            name="address"
                            id=""
                            placeholder="C/ Falsa 123"
                            defaultValue={form.address}
                            {...register("address", {
                                required: true,
                                minLength: 4,
                                maxLength: 128,
                            })}
                        />
                        {errors.address?.type === "required" && (
                            <span className="text-sm ml-2">
                                *This field is required
                            </span>
                        )}
                        {errors.address?.type === "minLength" && (
                            <span className="text-sm ml-2">
                                *This address is too short
                            </span>
                        )}
                        {errors.address?.type === "maxLength" && (
                            <span className="text-sm ml-2">
                                *This address is too long
                            </span>
                        )}
                        <label htmlFor="description">Description</label>
                        <textarea
                            className="input-class"
                            type="text"
                            name="description"
                            id=""
                            rows="3"
                            placeholder="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque laoreet ultricies maximus. Vivamus molestie felis vitae lectus dignissim efficitur. Mauris tincidunt efficitur arcu in interdum. Sed tempus est vitae volutpat egestas. Maecenas ut ex id ex eleifend cursus."
                            defaultValue={form.description}
                            {...register("description")}
                        />
                        <label htmlFor="type">Type*</label>

                        <select
                            className="input-class text-black capitalize"
                            name="type"
                            defaultValue={form.type}
                            required
                            {...register("type", {
                                required: true,
                            })}
                        >
                            {types.map((type) => (
                                <option
                                    className="capitalize"
                                    key={type}
                                    value={type}
                                >
                                    {type}
                                </option>
                            ))}
                        </select>
                        {errors.type && (
                            <span className="text-sm ml-2">
                                *This field is required
                            </span>
                        )}

                        <input
                            className="hidden"
                            ref={hiddenInput}
                            type="file"
                            multiple
                            accept="image/*"
                            onChange={(e) => loadImage(e.target.files)}
                        />
                        <label htmlFor="type">Photos</label>
                        <div className="flex flex-col lg:flex-row gap-x-6 gap-y-2 lg:gap-y-0">
                            <div
                                className="flex bg-transparent text-white p-4 h-min items-center justify-center border-lg rounded-full shadow-lg border-2 cursor-pointer hover:border-white hover:bg-white hover:text-company transition-all"
                                onClick={handleClick}
                            >
                                +Add photo
                            </div>
                            <div className="flex flex-1 gap-x-2 overflow-hidden">
                                {images.map((image, idx) => {
                                    return (
                                        <img
                                            className="w-1/2 lg:max-w-32 shadow-lg"
                                            src={image}
                                            alt="img"
                                            key={"img" + idx}
                                        />
                                    );
                                })}
                            </div>
                        </div>
                    </div>

                    <button
                        className="btn-class mt-auto w-1/2 self-center"
                        type="submit"
                        disabled={!isDirty || !isValid}
                    >
                        Next step
                    </button>
                </div>
            </form>
        </div>
    );
}
