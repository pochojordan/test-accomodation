// Toastify
import { showNotification } from "/src/utils/global.js";

import React from "react";

interface TSProps {
    nextStep: void;
    form: object;
}

export default function ThirdStep({ nextStep, form }: TSProps) {
    // ultimo paso
    const finish = () => {
        showNotification(true);
        console.log("This is the result of form :>> ", form);
    };

    return (
        <div className="flex flex-col h-full overflow-hidden gap-y-2 step">
            <div className="flex flex-col flex-1 gap-y-2 module">
                <h2 className="title text-2xl">Accomodation</h2>
                {Object.keys(form).map((key, idx) => {
                    let res = null;
                    if (key == "images") {
                        res = !key.includes("owner_") ? (
                            <div className="flex flex-col ml-2 gap-y-1 overflow-hidden">
                                <div>
                                    <span className="font-bold capitalize">
                                        {key}
                                    </span>
                                    :
                                </div>
                                <div className="flex gap-x-2">
                                    {form[key].map((image, idx) => {
                                        return (
                                            <img
                                                className="max-w-32 shadow-lg"
                                                src={image}
                                                alt="img"
                                                key={"img" + idx}
                                            />
                                        );
                                    })}
                                </div>
                            </div>
                        ) : null;
                    } else {
                        res = !key.includes("owner_") ? (
                            <div className="flex ml-2" key={key + "_" + idx}>
                                <div>
                                    <span className="font-bold capitalize">
                                        {key}
                                    </span>
                                </div>
                                <span>: {form[key]}</span>
                            </div>
                        ) : null;
                    }
                    return res;
                })}
                <h2 className="title text-2xl mt-6">Owner</h2>
                {Object.keys(form).map((key, idx) => {
                    return key.includes("owner_") ? (
                        <div className="flex ml-2" key={key + "_" + idx}>
                            <div>
                                <span className="font-bold capitalize">
                                    {key.replace("owner_", "")}
                                </span>
                            </div>
                            <span>: {form[key]}</span>
                        </div>
                    ) : null;
                })}

                <button
                    className="btn-class mt-auto w-1/2 self-center"
                    onClick={finish}
                >
                    Submit
                </button>
            </div>
        </div>
    );
}
