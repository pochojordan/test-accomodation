// archivo de funciones globales

// funcion para mostrar notificacion aleatoria
import { toast } from "react-toastify";

const showNotification = (rnd = false, msg) => {
    const options = {
        position: "top-right",
        autoClose: 2000,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "dark",
    };

    if (rnd) {
        Math.random() < 0.5
            ? toast.success("Submitted form!", options)
            : toast.error("Failed form!", options);
    } else {
        toast.info(msg, options);
    }
};

export { showNotification };
